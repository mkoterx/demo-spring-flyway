package com.mkoterx.demospringflyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringFlywayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringFlywayApplication.class, args);
    }
}
