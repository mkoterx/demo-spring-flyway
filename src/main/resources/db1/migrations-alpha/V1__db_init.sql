CREATE TABLE public.customers (
  id BIGSERIAL PRIMARY KEY,
  first_name TEXT,
  last_name TEXT,
  age INTEGER
);